var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('serve', function() {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});

	gulp.watch('index.html', browserSync.reload);
	gulp.watch('css/*.css', browserSync.reload);
	gulp.watch('js/*.js', browserSync.reload);
});

gulp.task('default', ['serve']);




//setting scroll value to 100
var scrollvalue = 100;

var tableWrapper = document.getElementById('table-wrapper');

document.getElementById('scroll-left').addEventListener('click', function () {
    tableWrapper.scrollLeft -= scrollvalue;
 
});

document.getElementById('scroll-right').addEventListener('click', function () {
    tableWrapper.scrollLeft += scrollvalue;
 
});
 
var columnKeys =[]; 

//load company data using fetch API

var companies = fetch('data/companies.json')
	.then(function(res) {
		if(res.status !== 200) {
			console.error('Cant load data' + res.status);
			return;
		}

		res.json().then(function(data){
		    companies = data.companies;
			populateData(companies);
			Object.keys(companies[0]).forEach(function(key){
				columnKeys.push(key);
			})
			
		});
	})
	.catch(function(err){
		console.error('Error occured!' + err);
	});


function populateData(company){

	company.map(function(c) {
        var row = document.createElement('tr');
       Object.keys(c).forEach(function(key){
          
         var cell = document.createElement('td');
         if(key !== 'description') {

            if(key === 'name'){
         	var p =  document.createElement('p');
         	p.innerText = c[key];
         	cell.append(p);
         	var small = document.createElement('small');
         	small.innerText = c.description;
         	cell.append(small);
         }

         else{
         	cell.append(document.createTextNode(c[key]));
         }
        
          row.append(cell);
          document.getElementById('company-table').append(row);

        }

       })

	});
}


function filter() {

    var filterList = document.getElementById('menu');
    var companyTable = document.getElementById('company-table');
	var allList = Array.from(filterList.getElementsByTagName('li'));
	
	 allList.map(function(li) {
		li.addEventListener('click', function() {
			var checkVal = li.firstElementChild.children[0];
			checkVal.checked = !checkVal.checked;

			 var allRows = companyTable.getElementsByTagName('tr');
			  var rows = Array.from(allRows);
			  for (i = 0; i <= rows.length; i++) {
				var th = allRows[0].getElementsByTagName('th')[i];

				for(var j = 0; j<rows.length; j++) {

					var cell = allRows[j].getElementsByTagName('td')[i];

					if(cell) {
						if((checkVal.value.toUpperCase()) === (th.innerText.toUpperCase())) {

							if(checkVal.checked === true) {
								th.style.display = 'none';
								allRows[j].getElementsByTagName('td')[i].style.display = 'none';
							} else {
								th.style.display = '';
								allRows[j].getElementsByTagName('td')[i].style.display = '';
							}
						}
					}							
				}
			}
	
		});
	});
	
}




	
	document.getElementById('dropdown').addEventListener('click', function() {
        
		document.querySelector('#dropmenu').style.display = 'block';

		Object.keys(columnKeys).forEach(function(key) {
		       if(columnKeys[key] !== 'name' && columnKeys[key]!== 'description') { 
				var li = document.createElement('li');
			 	var a = document.createElement('a');
			    var input = document.createElement('input');
				input.type = 'checkbox';
				input.value = columnKeys[key];
				input.checked = false;
				input.id = columnKeys[key];
				a.innerText = columnKeys[key];
				a.style.textTransform = "uppecase";
				input.style.marginRight = "10px";
				a.prepend(input);
			 	li.append(a);
			 	
				document.getElementById('menu').append(li);
			}
		});	

		 filter();
	});



document.getElementById('close-dropdown').addEventListener('click', function() {
	document.querySelector('#dropmenu').style.display = 'none';
	document.getElementById('menu').innerHTML = "";
});





